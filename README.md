# atreides
This keyboard is a derivative of the [Atreus](https://gitlab.com/technomancy/atreides) keyboard with the notable addition of an OLED screen.

There is no pre-made PCB, it must be hardwired.

From [Wikipedia](https://en.wikipedia.org/wiki/atreus):

>The word Atreides refers to one of the sons of Atreus—Agamemnon and Menelaus. The plural form Atreidae or Atreidai refers to both sons collectively; in English, the form Atreides (the same form as the singular) is often used.

## Case
The case design is written in OpenSCAD with the intention of laser cutting it, and is easily tweakable to your liking with the variables included in the top of the file. The main parameters are:
  - `angle`, one half of the angle between the left and the right groups of keys
  - `n_rows`, `n_cols`, number of rows and columns to use
  - `staggering_offsets`, column staggering offsets
  - `cable_hole_width`, width of the hole for the USB cable
  - `hand_separation`, distance between the left and the right groups of keys
  - `screw_hole_radius`, radius of screw holes
  - `use_notched_holes` (boolean), whether notched Cherry MX switch holes are desired

See the comments in `atreides_case.scad` for details.

To use, open this script in OpenSCAD, select **Compile and Render (CGAL)** in the **Design** menu, then select **Export as DXF...**.

Wood cases should be finished with sandpaper and lacquer, shellac, or polyurethane.

## Bill of Materials
 - 44 Cherry compatible switches
 - 50 diodes
 - 44 1u keycaps (flat profiles preferred)
 - Pro Micro compatible microcontroller without headers
 - Case materials
 - Micro-USB cable

## License
Copyright © 2014-2020 Phil Hagelberg and contributors, 2021 Armaan Bhojwani

Released under the GNU GPLv3, see the LICENSE file for more information.
